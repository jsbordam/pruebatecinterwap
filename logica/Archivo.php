<?php  
    //Para importar las clases que necesito usar:
    require "persistencia/ArchivoDAO.php";
    class Archivo
    {
        private $idArchivo;
        private $email;
        private $nombre;
        private $apellido;
        private $codigo;

        private $conexion;
        private $archivoDAO;
        
        //Constructor:

        function Archivo ($pidArchivo="", $pEmail="", $pNombre="", $pApellido="", $pCodigo="")
        {
            $this -> idArchivo = $pidArchivo;
            $this -> email = $pEmail;
            $this -> nombre = $pNombre;    
            $this -> apellido = $pApellido;    
            $this -> codigo = $pCodigo; 
            
            $this -> conexion = new Conexion();
            $this -> archivoDAO = new ArchivoDAO ($pidArchivo, $pEmail, $pNombre, $pApellido, $pCodigo);   
        }
        
        
        //Metodos GET:
        
        public function getIdArchivo()
        {
            return $this->idArchivo;
        }
        
        public function getEmail()
        {
            return $this->email;
        }
        
        public function getNombre()
        {
            return $this->nombre;
        }

        public function getApellido()
        {
            return $this->apellido;
        }

        public function getCodigo()
        {
            return $this->codigo;
        }


        //Metodos para conectar con las consultas de la BD:
        
        
        //Metodo para crear un registro:
        function crear()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> archivoDAO -> crear();
            $this -> conexion -> ejecutar($this -> archivoDAO -> crear());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }


        //Metodo para consultar un regsitro:
        function consultar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> archivoDAO -> consultar();
            $this -> conexion -> ejecutar($this -> archivoDAO -> consultar());
            $this -> conexion -> cerrar(); //Se cierra la conexion

            if ($this -> conexion -> numFilas() == 0)//No existe el registro
            {

            }
            else
            {
                $resultado = $this -> conexion -> extraer();
                $this -> email = $resultado[0];
                $this -> nombre = $resultado[1];
                $this -> apellido = $resultado[2];
                $this -> codigo = $resultado[3];
            }    
        }

        //Metodo para consultar todos los registros:
        function consultarTodos($filtro)
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> archivoDAO -> consultarTodos($filtro);
            $this -> conexion -> ejecutar($this -> archivoDAO -> consultarTodos($filtro));
            $this -> conexion -> cerrar(); //Se cierra la conexion
            $registros = array();
            //Para que itere tantas veces como hayan registros:
            while (($resultado = $this -> conexion -> extraer()) != null)
            {
                array_push($registros, new Archivo ($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4]));
            }
            return $registros;
        }
        
        //Metodo para editar un registro:
        function editar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> archivoDAO -> editar();
            $this -> conexion -> ejecutar($this -> archivoDAO -> editar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        }  
        
        //Metodo para eliminar un registro:
        function eliminar()
        {
            $this -> conexion -> abrir(); //Se abre la conexion
            //Para ver la consulta sql = echo $this -> archivoDAO -> eliminar();
            $this -> conexion -> ejecutar($this -> archivoDAO -> eliminar());
            $this -> conexion -> cerrar(); //Se cierra la conexion
        } 
    }
?>