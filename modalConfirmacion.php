<div class="modal-header">
	<font face="Alergan" size="5" color="red">ATENCIÓN <i class="fas fa-exclamation-triangle"></i></font>
</div>
<div class="modal-body">
	<h5 class="modal-title">¿Seguro que quieres eliminar este usuario?</h5>
</div>
<div class="modal-footer">
	<a href="index.php?pid=<?php echo base64_encode("presentacion/registros.php")?>&eliminar&id=<?php echo $_GET["id"]?>" class="btn login_btn btn-block">CONFIRMAR</a>
	<button type="button" class="btn login_btn2 btn-block" data-dismiss="modal">CANCELAR</button>
</div>
