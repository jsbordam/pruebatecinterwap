<?php 
    $error = 0;

    if(isset($_POST["subir"]))
    {
        //Para validar tipo de archivo:
        
        $archivo = $_FILES["archivo"]["name"];
        $nombreArchivo = $_FILES["archivo"]["tmp_name"];

        $extension = substr($archivo, strrpos($archivo, "."));
        
        $error = ($extension == ".txt") ? 0 : 1;

        if($error == 0)
        {
            //Se guarda archivo en directorio:
            $error = (move_uploaded_file($nombreArchivo, "archivos/$archivo")) ? 0 : 2;

            //Se lee el archivo para acceder a la información:
            $arch = fopen("archivos/$archivo", "r")
            or die("Error al abrir el archivo");

            while($line = fgetcsv($arch,0,",")) 
            {
                //Procesar con un for, cada columna en esta fila
                for ($i = 0, $j = count($line); $i < $j; $i++) 
                {
                  switch ($i) 
                  {
                    case 0: //Sabemos que la primera columna es el email
                        $email = $line[$i];
                    break;

                    case 1: //Sabemos que la segunda columna es el nombre
                        $nombre = ($line[$i] == "") ? "No" : $line[$i];
                    break;

                    case 2: //Sabemos que la tercera columna es el apellido
                        $apellido = ($line[$i] == "") ? "No" : $line[$i];
                    break;

                    case 3: //Sabemos que la cuarta columna es el codigo 

                        //Validación de campo codigo obligatorio:
                        ($line[$i] == "1" || $line[$i] == "2" || $line[$i] == "3") ? $codigo = $line[$i] : $error = 3;                                         
                    break;

                    default:
                    break;
                  }
                }
                if ($error == 0)
                {
                    //Se crea el registro en la Base de Datos:
                    $archivo = new Archivo("", $email, $nombre, $apellido, $codigo);
                    $archivo -> crear();
                }
            }

            fclose($arch) or die("Error al cerrar el archivo");
        }
    }
?>

<div class="container">
    <?php include "presentacion/encabezado.php"; ?>
    <div class="row mt-5">
        <div class="col-lg-7">
            <div class="card">
                <div class="card-header">
                    <h3 style="color:white;">Prueba Tecnica para Desarrollador PHP</h3>
                </div>
                <div class="card-body">
                    <img src="https://interwap.co/logos/interwaplogoblue.png" width="100%">
                </div>
            </div>
        </div>

        <div class="col-lg-5">
            <div class="card">
                <div class="card-header">
                    <h3 style="color:white;">Carga tu archivo plano aqui!</h3>
                </div>
                <div class="card-body">
                    <?php
                            if($error == 1)
                            { 
                        ?>
                    <section id="alert1">
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-exclamation-triangle"></i> El archivo debe ser de tipo
                                (.txt)</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>
                    <?php
                        }                      
                    ?>

                    <?php
                        if($error == 2)
                        { 
                    ?>
                    <section id="alert1">
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-exclamation-triangle"></i> No se pudo guardar el archivo,
                                hubo un error en la carga...</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>
                    <?php
                        }                      
                    ?>

                    <?php
                        if($error == 3)
                        { 
                    ?>
                    <section id="alert1">
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-exclamation-triangle"></i> El archivo no tiene el formato
                                correcto</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>
                    <?php
                        }                      
                    ?>

                    <?php
                        if(isset($_POST["subir"]) && $error == 0)
                        { 
                    ?>
                    <section id="alert2">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <strong><i class="fas fa-check-circle"></i> Archivo cargado con éxito!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </section>
                    <?php
                        }                      
                    ?>

                    <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/inicio.php")?> method="post"
                        enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" name="archivo" class="form-control" required="required">
                        </div>

                        <div class="form-group">
                            <input type="submit" name="subir" value="Subir Archivo" class="btn login_btn btn-block">
                        </div>
                        <div class="form-group">
                            <a class="btn login_btn2 btn-block"
                                href="index.php?pid=<?php echo base64_encode("presentacion/registros.php")?>">
                                Ver Registros</a>
                        </div>
                    </form>
                    <br>
                    <p>
                        Desarrollado por: JONATHAN BORDA
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>