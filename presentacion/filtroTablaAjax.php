<?php 
    //Se hace consulta a BD de todos los registros con el filtro:

    $archivo = new Archivo();
    $registros = $archivo -> consultarTodos($_GET["filtro"]);
?>

<?php 
    if ($_GET["filtro"] == "1")
    {
?>
<br>
<h2 style="font-family:arial black; color:#ff3300; text-align: center;">USUARIOS ACTIVOS</h2>
<br>
<?php 
    }
    else if ($_GET["filtro"] == "2")
    {
?>
<br>
<h2 style="font-family:arial black; color:#ff3300; text-align: center;">USUARIOS INACTIVOS</h2>
<br>
<?php 
    }
    else
    {
?>
<br>
<h2 style="font-family:arial black; color:#ff3300; text-align: center;">USUARIOS EN PROCESO DE ESPERA</h2>
<br>
<?php 
    }
?>
<table class="table table-light table-striped table-hover">
    <thead>
        <tr>
            <th class="text-center">
                <h5>
                    <font face='Arial Black' size='3' color='black'>Registro N°</font>
                </h5>
            </th>
            <th class="text-center">
                <h5>
                    <font face='Arial Black' size='3' color='black'>Email</font>
                </h5>
            </th>
            <th class="text-center">
                <h5>
                    <font face='Arial Black' size='3' color='black'>Nombre</font>
                </h5>
            </th>
            <th class="text-center">
                <h5>
                    <font face='Arial Black' size='3' color='black'>Apellido</font>
                </h5>
            </th>
            <th class="text-center">
                <h5>
                    <font face='Arial Black' size='3' color='black'>Código</font>
                </h5>
            </th>
            <th class="text-center">
                <h5>
                    <font face='Arial Black' size='3' color='black'>Acciones</font>
                </h5>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php 
            foreach ($registros as $registroActual)
            {                  	   
                echo "<tr>";
                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $registroActual->getIdArchivo() . "</font></h5></td>";             	        
                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $registroActual->getEmail() . "</font></h5></td>";
                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $registroActual->getNombre() . "</font></h5></td>";
                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $registroActual->getApellido() . "</font></h5></td>";
                    echo "<td class='text-center'><h5><font face='Arial' size='3' color='black'>" . $registroActual->getCodigo() . "</font></h5></td>";
                    
                    echo "<td class='text-center'>
                            <a href='index.php?pid=" . base64_encode("presentacion/editar.php") . "&id=" . $registroActual->getIdArchivo() . "'><font size='5' color='red'><i class='fas fa-edit' data-toggle='tooltip' data-placement='bottom' title='Editar'></i></font></a>
                            <a href='modalConfirmacion.php?id=". $registroActual->getIdArchivo() ."' data-toggle='modal' data-target='#modalConfirm'><font size='5' color='red'><i class='fas fa-trash-alt' data-toggle='tooltip' data-placement='bottom' title='Eliminar'></i></font></a>        
                        </td>";
                echo "</tr>";                	   
            }              
        ?>
    </tbody>
</table>

<a class="btn login_btn btn-block" href="index.php?pid=<?php echo base64_encode("presentacion/crear.php")?>">
    Crear nuevo usuario</a>


<!--Este es el Modal de confirmacion de eliminar: -->

<section id="modalC">
    <div class="modal fade" id="modalConfirm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="modal-content"></div>
        </div>
    </div>
</section>

<!--Este JavaScript es para confirmacion: -->
<script>
$('body').on('show.bs.modal', '.modal', function(e) {
    var link = $(e.relatedTarget);
    $(this).find(".modal-content").load(link.attr("href"));
});
</script>