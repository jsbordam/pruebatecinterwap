<?php 
    if (isset($_GET["eliminar"]))
    {
        //Se elimina el registro de la BD:
        $idArchivo = $_GET["id"];
        $archivo = new Archivo($idArchivo);
        $archivo->eliminar();
    }
?>


<div class="container">
    <?php include "presentacion/encabezado.php"; ?>
    <div class="row mt-5">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 style="color:white;">Registros almacenados</h3>
                </div>

                <div class="card-body">

                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label class="input-group-text" style="color:white;">Genera la tabla por filtro</label>
                            </div>
                            <select class="custom-select" id="filtro">
                                <option value="-1">Seleccionar filtro</option>
                                <option value="1">Usuarios activos</option>
                                <option value="2">Usuarios inactivos</option>
                                <option value="3">Usuarios en proceso de espera</option>
                            </select>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div id="resultados">

                            </div>
                        </div>
                    </div>
                    <a class="btn login_btn2 btn-block"
                        href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>">
                        Volver atrás</a>
                </div>
            </div>
        </div>
    </div>




    <!--Este es el JQuery para cargar tabla por filtro: -->
    <script>
    $(document).ready(function() {
        $("#filtro").change(function() {
            if ($("#filtro").val() != -1) {
                //Encriptamos la ruta para que no sea visible en la inspección de codigo:
                url =
                    "indexAjax.php?pid=<?php echo base64_encode("presentacion/filtroTablaAjax.php") ?>&filtro=" +
                    $("#filtro").val();
                $("#resultados").load(url);
            }
        });
    });
    </script>