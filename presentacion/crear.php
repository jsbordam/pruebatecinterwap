<?php   
    if (isset($_POST["crear"]))
    {
        //Recojo los datos que llegan por POST del formulario:
        $email = $_POST["email"];
        $nombre = ($_POST["nombre"] == "") ? "No" : $_POST["nombre"];
        $apellido = ($_POST["apellido"] == "") ? "No" : $_POST["apellido"];
        $codigo = $_POST["codigo"];

        //Se crea el registro en la Base de Datos:
        $archivo = new Archivo("", $email, $nombre, $apellido, $codigo);
        $archivo -> crear();     
    }  
?>

<div class="container">
    <?php include "presentacion/encabezado.php"; ?>
    <div class="row mt-5">
        <div class="col">
            <section id="edit">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-9">
                                <h3 style="color:white;">Creando Usuarios</h3>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <!-- Para avisar que se creo correctamente-->
                        <?php
                            if(isset($_POST["crear"]))
                            { 
                        ?>
                        <section id="alert2">
                            <div class="alert alert-dismissible fade show" role="alert">
                                <strong><i class="fas fa-check-circle"></i> Usuario creado correctamente!</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </section>
                        <?php 
                            }
                        ?>

                        <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/crear.php")?>
                            method="post" enctype="multipart/form-data">

                            <div class="form-row">
                                <div class="form-group col-sm-3">
                                    <label>Email:</label> <input type="email" name="email" class="form-control"
                                        placeholder="Escribe un email" required="required">
                                </div>
                                <div class="form-group col-sm-3">
                                    <label>Nombre:</label> <input type="text" name="nombre" class="form-control"
                                        placeholder="Escribe un nombre">
                                </div>
                                <div class="form-group col-sm-3">
                                    <label>Apellido:</label> <input type="text" name="apellido" class="form-control"
                                        placeholder="Escribe un apellido">
                                </div>
                                <div class="form-group col-sm-3">
                                    <label for="inputState">Codigo:</label> <select id="inputState" class="form-control"
                                        name="codigo">
                                        <option value="1">Usuario activo</option>
                                        <option value="2">Usuario inactivo</option>
                                        <option value="3">Usuario en proceso de espera</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">

                            </div>

                            <div class="form-group">
                                <input type="submit" name="crear" value="Crear" class="btn login_btn btn-block">
                            </div>
                            <a class="btn login_btn2 btn-block"
                                href="index.php?pid=<?php echo base64_encode("presentacion/registros.php")?>">
                                Volver atrás</a>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>