<?php
    class ArchivoDAO
    {
        private $idArchivo;
        private $email;
        private $nombre;
        private $apellido;
        private $codigo;
        
        //Constructor:
        
        function ArchivoDAO ($pidArchivo="", $pEmail="", $pNombre="", $pApellido="", $pCodigo="")
        {
            $this -> idArchivo = $pidArchivo;
            $this -> email = $pEmail;
            $this -> nombre = $pNombre;    
            $this -> apellido = $pApellido;    
            $this -> codigo = $pCodigo;    
        }
        
        //Metodos para las consultas en la BD:
        
        //Para crear:
        function crear()
        {
            return "INSERT into archivo (email, nombre, apellido, codigo)
                    VALUES ('" . $this -> email . "', '" . $this -> nombre . "', '" . $this -> apellido . "', '" . $this -> codigo . "');";
        }     

        //Para consultar uno:
        function consultar()
        {
            return "SELECT email, nombre, apellido, codigo
                    FROM archivo
                    WHERE idArchivo = '" . $this -> idArchivo . "'";
        }
         

        //Para consultar todos:
        function consultarTodos($filtro)
        {
            return "SELECT idArchivo, email, nombre, apellido, codigo
                    FROM archivo
                    WHERE codigo = '" . $filtro . "'";
        }
        
        //Para editar:
        function editar()
        {
            return "UPDATE archivo
                    SET email = '". $this -> email . "', nombre = '". $this -> nombre . "', apellido = '". $this -> apellido . "', codigo = '". $this -> codigo . "'
                    WHERE idArchivo = '" . $this -> idArchivo . "'";
        }
        
        
        //Para eliminar:
        function eliminar()
        {
            return "DELETE FROM archivo                
                    WHERE idArchivo = '" . $this -> idArchivo . "'";
        }
    }
?>
