<!-- Estas lineas es para implementar tooltip (Pequeñas etiquetas descriptivas de botones) -->
<script type="text/javascript">
	$(function () 
	{
      $('[data-toggle="tooltip"]').tooltip()
    })
</script>   
<?php  
    require "logica/Archivo.php";
    require "persistencia/Conexion.php";
    
    //Desencriptamos el pid para que lo pueda leer:
    $pid = base64_decode($_GET["pid"]);
    include $pid;
?>